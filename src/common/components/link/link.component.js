import styled from '@emotion/styled';
import theme from '../../helpers/theme';

const Link = styled.a`
  color: ${theme('colors.primary')};
`;

Link.defaultProps = {
  defaultTheme: {
    colors: {
      primary: '#ff6007'
    }
  }
};

export default Link;
