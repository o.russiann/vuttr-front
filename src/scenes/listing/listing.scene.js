/* eslint-disable react/jsx-no-undef */
import React from 'react';
import styled from '@emotion/styled';
import {css} from '@emotion/core';
import PropTypes from 'prop-types';
/** components */
import isEmpty from '../../common/helpers/is-empty.js';
import SearchField from '../../common/components/searchfield/searchfield.component';
import Checkbox from '../../common/components/checkbox/checkbox.component';
import Button from '../../common/components/button/button.component';
import Modal from '../../common/components/modal/modal.component';
import Preloader from '../../common/components/preloader/preloader.component';
/** scene components */
import ToolItem from './components/tool-item/tool-item.component';
/** scenes */
import NewScene from '../new';
/** partials */
import Header from './partials/header';
import EmptyState from './partials/empty-state';

/**
|--------------------------------------------------
| Custom Styles
|--------------------------------------------------
*/

const tagsCheckboxStyle = css`
  flex: 1;
  margin-left: 15px;

  @media (max-width: 600px) {
    margin-top: 10px;
    margin-left: 10px;
    display: block;
  }
`;

const cancelButtonStyle = css`
  background: #f5f5f5;
  color: #212121;
  border: 1px solid #bdbdbd;
  margin-right: 10px;
`;

/**
|--------------------------------------------------
| Layout Elements
|--------------------------------------------------
*/

const Toolbar = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
`;

const Container = styled.div`
  max-width: 960px;
  margin: 0 auto;
`;

const ConfirmModalFooter = styled.div`
  display: flex;
  flex-flow: row-reverse;
`;

const ConfirmModalText = styled.p`
  margin-top: 0;
`;

const PreloaderContent = styled.div`
  display: flex;
  height: 300px;
  justify-content: center;
  align-items: center;
`;

/**
|--------------------------------------------------
| State
|--------------------------------------------------
*/

const initialState = {
  isFetching: true,
  data: [],
  searchText: '',
  isFilterOnlyInTags: false,
  setSearchText: '',
  newToolModalOpened: false,
  toggleFilterOnlyInTags: () => {},
  toggleNewToolModal: () => {},
  confirmModal: {
    opened: false,
    tool: {
      title: ''
    }
  },
  remove: () => {},
  openConfirmModal: () => {},
  closeConfirmModal: () => {},
  onTagClick: () => {},
  onSearchText: () => {}
};

/**
|--------------------------------------------------
| Scene
|--------------------------------------------------
*/

const ToolsScene = ({
  isFetching,
  data,
  isFilterOnlyInTags,
  newToolModalOpened,
  confirmModal,
  remove,
  toggleFilterOnlyInTags,
  toggleNewToolModal,
  openConfirmModal,
  closeConfirmModal,
  onTagClick,
  onSearchText
}) => {
  return (
    <Container>
      <Header />
      <Toolbar>
        <div>
          <SearchField onSearch={onSearchText} aria-label="Search field" />
          <Checkbox
            label="Search in tags only"
            checked={isFilterOnlyInTags}
            onChange={() => toggleFilterOnlyInTags()}
            css={tagsCheckboxStyle}
            aria-label="Search in tags only"
          />
        </div>
        <Button onClick={toggleNewToolModal}> add </Button>
      </Toolbar>

      <main>
        <Choose>
          <When condition={isFetching}>
            <PreloaderContent>
              <Preloader />
            </PreloaderContent>
          </When>
          <When condition={isEmpty(data)}>
            <EmptyState />
          </When>
          <Otherwise>
            {data.map(tool => (
              <ToolItem
                key={tool.id}
                tool={tool}
                onTagClick={onTagClick}
                onRemoveClick={openConfirmModal}
              />
            ))}
          </Otherwise>
        </Choose>
      </main>

      <Modal
        isVisible={newToolModalOpened}
        title="Nova Tool"
        onClose={toggleNewToolModal}
      >
        <NewScene />
      </Modal>

      <Modal
        isVisible={confirmModal.opened}
        title="Remove Tool"
        onClose={closeConfirmModal}
      >
        <ConfirmModalText>
          Are you sure you want to remove {confirmModal.tool.title}?
        </ConfirmModalText>
        <ConfirmModalFooter>
          <Button onClick={remove}>Yes, remove</Button>
          <Button onClick={closeConfirmModal} css={cancelButtonStyle}>
            Cancel
          </Button>
        </ConfirmModalFooter>
      </Modal>
    </Container>
  );
};

ToolsScene.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object).isRequired,
  isFetching: PropTypes.bool.isRequired,
  isFilterOnlyInTags: PropTypes.bool.isRequired,
  newToolModalOpened: PropTypes.bool.isRequired,
  confirmModal: PropTypes.object.isRequired,
  remove: PropTypes.func.isRequired,
  toggleFilterOnlyInTags: PropTypes.func.isRequired,
  toggleNewToolModal: PropTypes.func.isRequired,
  openConfirmModal: PropTypes.func.isRequired,
  closeConfirmModal: PropTypes.func.isRequired,
  onTagClick: PropTypes.func.isRequired,
  onSearchText: PropTypes.func.isRequired
};

ToolsScene.defaultProps = initialState;

export default ToolsScene;
