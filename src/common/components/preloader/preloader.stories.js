import React from 'react';

import {storiesOf} from '@storybook/react';

import Preloader from './preloader.component';

storiesOf('Preloader', module)
  .addParameters({jest: ['preloader']})
  .add('default', () => <Preloader />);
