import styled from '@emotion/styled';
import theme from '../../helpers/theme';

const Tag = styled.span`
  height: fit-content;
  background: ${theme('colors.primaryDeepLight')};
  padding: 0 2px;
  margin-right: 5px;
  color: ${theme('secondaryTextColor')};
  cursor: pointer;
  border-radius: 2px;
`;

Tag.defaultProps = {
  defaultTheme: {
    secondaryTextColor: '#212121',
    colors: {
      primaryDeepLight: '#bbdefb'
    }
  }
};

export default Tag;
