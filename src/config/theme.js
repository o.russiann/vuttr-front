const theme = {
  fontFamily: 'Ubuntu, system-ui',
  textColor: '#fff',
  secondaryTextColor: '#212121',
  colors: {
    primary: '#1976d2',
    primaryLight: '#63a4ff',
    primaryDeepLight: '#bbdefb',
    primaryDark: '#004ba0',
    secondary: 'red'
  }
};

export default theme;
