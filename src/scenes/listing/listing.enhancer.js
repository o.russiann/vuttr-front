/* eslint-disable react-hooks/rules-of-hooks */
import {useSelector, useDispatch} from 'react-redux';
import {useMount} from 'react-use';
import compose from '../../common/helpers/compose';
import withHooks from '../../common/helpers/with-hooks';
import withMemo from '../../common/helpers/with-memo';
import equals from '../../common/helpers/equals';

export default compose(
  withHooks(props => {
    /** state */
    const tools = useSelector(state => state.tools.asMutable({deep: true}));
    const listing = useSelector(state => state.listing.asMutable({deep: true}));

    const state = {
      data: tools.data,
      isFetching: tools.loadings.fetching,
      isFilterOnlyInTags: tools.filters.filterOnlyInTags,
      newToolModalOpened: listing.newToolModalOpened,
      confirmModal: listing.confirmModal
    };

    /** actions */
    const models = useDispatch();

    const actions = {
      setSearchText: models.tools.setSearchText,
      toggleFilterOnlyInTags: models.tools.toggleFilterOnlyInTags,
      onSearchText: models.tools.setSearchText,
      onTagClick: models.tools.setSearchText,
      toggleNewToolModal: models.listing.toggleNewToolModal,
      openConfirmModal: models.listing.openConfirmModal,
      closeConfirmModal: models.listing.closeConfirmModal,
      remove: () => {
        models.tools.remove({id: state.confirmModal.tool.id});
      }
    };

    /** side effects */
    useMount(models.tools.find);

    /** props */
    return {
      ...state,
      ...actions,
      ...props
    };
  }),
  withMemo(equals)
);
