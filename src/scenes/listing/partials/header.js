import React from 'react';
import styled from '@emotion/styled';
import theme from '../../../common/helpers/theme';

const Logo = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width="24"
    height="24"
    viewBox="0 0 24 24"
    fill="none"
    stroke="currentColor"
    strokeWidth="2"
    strokeLinecap="round"
    strokeLinejoin="round"
  >
    <path d="M2 3h6a4 4 0 0 1 4 4v14a3 3 0 0 0-3-3H2z" />
    <path d="M22 3h-6a4 4 0 0 0-4 4v14a3 3 0 0 1 3-3h7z" />
  </svg>
);

const Title = styled.div`
  font-size: 23px;
  letter-spacing: 6px;
  display: flex;
  align-items: center;
`;

const Subtitle = styled.div``;

const LogoIcon = styled.div`
  width: 40px;
  height: 40px;
  border: 2px solid ${theme('colors.primary')};
  border-radius: 2px;
  margin-right: 10px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Container = styled.div`
  display: flex;
  align-content: center;
  padding-top: 15px;
  padding-bottom: 15px;
`;

const Header = () => (
  <Container>
    <LogoIcon>
      <Logo />
    </LogoIcon>
    <div>
      <Title>VUTTR</Title>
      <Subtitle>Very Useful Tools to Remember</Subtitle>
    </div>
  </Container>
);

export default Header;
