/* istanbul ignore file */
import React from 'react';
import {Provider} from 'react-redux';
import {ThemeProvider} from 'emotion-theming';
import {Global, css} from '@emotion/core';
import ToolsScene from './scenes/listing';
import store from './config/store';
import theme from './config/theme';

function App() {
  return (
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <Global
          styles={css`
            body {
              background: #fafdff;
              font-family: 'Ubuntu', sans-serif;
            }
          `}
        />
        <ToolsScene />
      </ThemeProvider>
    </Provider>
  );
}

export default App;
