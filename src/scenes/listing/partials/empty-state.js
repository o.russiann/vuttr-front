import React from 'react';
import styled from '@emotion/styled';
import Button from '../../../common/components/button/button.component';

const Container = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  height: 300px;
  justify-content: center;
`;

const Text = styled.div`
  margin-bottom: 20px;
`;

const EmptyState = ({onActionClick}) => (
  <Container>
    <Text>Ops! No tools found.</Text>
    <Button onClick={onActionClick}>Create a new one :)</Button>
  </Container>
);

export default EmptyState;
