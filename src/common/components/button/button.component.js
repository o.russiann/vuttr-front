import styled from '@emotion/styled';
import theme from '../../helpers/theme';

const Button = styled.button`
  font-family: 'Ubuntu', sans-serif;
  font-size: 12px;
  color: ${theme('textColor')};
  background-color: ${theme('colors.primary')};
  text-align: center;
  text-transform: uppercase;
  padding: 10px 50px;
  border: none;
  border-radius: 30px;
  outline: none;
  cursor: pointer;

  :hover {
    opacity: 0.9;
  }
`;

Button.defaultProps = {
  'data-testid': 'button',
  defaultTheme: {
    textColor: '#fff',
    colors: {
      primary: '#1976d2'
    }
  }
};

export default Button;
