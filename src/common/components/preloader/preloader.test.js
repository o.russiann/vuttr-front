import React from 'react';
import {render, cleanup} from '@testing-library/react';

import Preloader from './preloader.component';

afterEach(cleanup);

it('snapshot', () => {
  const {asFragment} = render(<Preloader />);
  expect(asFragment()).toMatchSnapshot();
});
