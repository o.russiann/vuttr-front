const get = (obj, keys = '') =>
  keys.split('.').reduce((xs, x) => (xs && xs[x] ? xs[x] : null), obj);

const theme = path => ({theme, defaultTheme}) =>
  get(theme, path) || get(defaultTheme, path);

export default theme;
