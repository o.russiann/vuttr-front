# VUTTR

## Install Dependencies

```bash
$ yarn || npm install
```

## Running Storybook

```bash
$ yarn storybook || npm run storybook
```

## Running Application

```bash
$ yarn start || npm start
```

## Running Test

```bash
$ yarn test || npm run test
$ yarn coverage || npm run coverage
```

## Architecture/Structure

The application is divided in some parts:

- **Common**
  - **Components**: Shared Components (UI)
  - **Helpers**: Utils, Custom Hooks, Custom HOCs, etc...
- **Models**: All business logic must be here in reducers, effects (Rematch) or Logics (middlware provided by redux-logic). It is Global and all scenes could have access through Enhancers.
- **Scenes**: Represent a scenario of the application. It not necessarily should be a page. It depends on how much business logic the worked interface will embed. For example, a modal could be a simple Partial or a separated Scenario.
  - **Scene Enhancer**: Mainly concerned to connect business logic to View without coupling it. Besides that, it could be used to enhance View using HOCs or Hooks (i18n, Lifecycle, Handlers, SideEffects, etc...)
  - **Scene Model**: Similar to Models but used to control view related state.
  - **Scene View**: A Stateless component without any behavioral controlling. Just JSX! This make test super easily.
  - **Scene Components**: Scenario related components. Sometimes we need to create component that are so specific that are used in only one scenario.
  - **Scene Partials**: Used to improve organization. Could be used to put forms, modals, tabs, tables, etc...
- **Services**: Focused to communicates with backend to fetch or send data.
- **Config**: General configuration purpose. (Redux Store, Emotion Theme, etc..)

## Stack

Production

- React
- Rematch(Redux)
- Redux-Logic
- Emotion
- Formik
- axios

Development & Testing

- Storybook
- Jest
- testing-library

Build

- react-app-rewired
- customize-cra
